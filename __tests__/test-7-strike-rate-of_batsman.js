const  strikeRateOfBatsman= require("../src/server/7-strike-rate-of_batsman");

test("Strike-Rate-Per-Person", () => {
  const testSampleDataMatches = [
    {
      id: 1,
      season: 2016,
    },
    {
      id: 2,
      season: 2015,
    },
    {
      id: 3,
      season: 2010,
    },
    {
      id: 4,
      season: 2016,
    },
    {
      id: 5,
      season: 2010,
    },
  ];

  const testSampleDataDeliveries = [
    {
      match_id: 1,
      extra_runs: 0,
      bowling_team: "KKR",
      batsman: "MS Dhoni",
      batsman_runs: 6,
      total_runs: 7,
      wide_runs: 0,
      bye_runs: 1,
      legbye_runs: 0,
      noball_runs: 0,
    },
    {
      match_id: 2,
      extra_runs: 0,
      bowling_team: "CSK",
      batsman: "MS Dhoni",
      batsman_runs: 2,
      total_runs: 0,
      wide_runs: 0,
      bye_runs: 0,
      legbye_runs: 0,
      noball_runs: 0,
    },
    {
      match_id: 3,
      extra_runs: 1,
      bowling_team: "MI",
      batsman: "MS Dhoni",
      batsman_runs: 2,
      total_runs: 3,
      wide_runs: 0,
      bye_runs: 1,
      legbye_runs: 0,
      noball_runs: 0,
    },
    {
      match_id: 4,
      extra_runs: 0,
      bowling_team: "CSK",
      batsman: "MS Dhoni",
      batsman_runs: 1,
      total_runs: 1,
      wide_runs: 0,
      bye_runs: 0,
      legbye_runs: 0,
      noball_runs: 0,
    },
    {
      match_id: 5,
      extra_runs: 0,
      bowling_team: "CSK",
      batsman: "MS Dhoni",
      batsman_runs: 4,
      total_runs: 4,
      wide_runs: 0,
      bye_runs: 0,
      legbye_runs: 0,
      noball_runs: 0,
    },
  ];

  const resultData = { 2010: "300.00", 2015: "200.00",2016: "350.00" };

  expect(
    strikeRateOfBatsman(testSampleDataDeliveries, testSampleDataMatches,"MS Dhoni")
  ).toEqual(resultData);
});
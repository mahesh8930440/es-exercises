function matchesWonPerTeamPerYear(allMatches){
  const allMatchesTeamWonPerYear = {};
  
  for (let index=0 ; index<allMatches.length ;index++){
      
    if (allMatchesTeamWonPerYear[allMatches[index].season] &&allMatchesTeamWonPerYear[allMatches[index].season][allMatches[index].winner]){
      allMatchesTeamWonPerYear[allMatches[index].season][allMatches[index].winner]++;
    } 
          
    else if(allMatchesTeamWonPerYear[allMatches[index].season] &&!allMatchesTeamWonPerYear[allMatches[index].season][allMatches[index].winner]){
      allMatchesTeamWonPerYear[allMatches[index].season][allMatches[index].winner] = 1;
    } 
          
    else {
            allMatchesTeamWonPerYear[allMatches[index].season] = {};
            allMatchesTeamWonPerYear[allMatches[index].season][allMatches[index].winner] = 1;
          }
    }
  return  allMatchesTeamWonPerYear;
}

module.exports=matchesWonPerTeamPerYear;


function mostNumberOfPlayerOfMatches(allMatches){
  const  allPlayerOfSeason={};

  for (let index=0;index<allMatches.length;index++){
      let season=allMatches[index].season;
      
      if(!(allPlayerOfSeason[season])){
          allPlayerOfSeason[season]={};

      }
      let playerOfMatch=allMatches[index].player_of_match;
      
      if(!(allPlayerOfSeason[season][playerOfMatch])){
          allPlayerOfSeason[season][playerOfMatch]=1;
      }

      else{
          allPlayerOfSeason[season][playerOfMatch] +=1;
      }
  }

  const heighestNumberOfPlayerOfMatches={}
  for (let keySeason in allPlayerOfSeason){
    
    const playerOfMatch=allPlayerOfSeason[keySeason];
    let count=0;
    let playerName=null;
    for(let keyPlayer in playerOfMatch){
      const numberPlayerOfMatches=playerOfMatch[keyPlayer] ;
      
      if (numberPlayerOfMatches>=count){
        playerName=keyPlayer;
        count=numberPlayerOfMatches;
        
      }
      
    }
    heighestNumberOfPlayerOfMatches[keySeason]=playerName;
  
  }
  return (heighestNumberOfPlayerOfMatches);
}  
     
module.exports=mostNumberOfPlayerOfMatches;    

